cmake_minimum_required(VERSION 3.14.0)

PROJECT(dolfincoefficients_bindings)

find_package(DOLFIN REQUIRED)
find_package(pybind11 REQUIRED)
find_package(dolfincoefficients REQUIRED)

include("${DOLFIN_USE_FILE}")

set(CMAKE_CXX_STANDARD 17)

if ("${pybind11_FOUND}")
    pybind11_add_module(dolfincoefficients_pybind SHARED dolfincoefficients/bindings.cpp)
    set(PYBIND11_CPP_STANDARD -std=c++17)
    target_include_directories(dolfincoefficients_pybind PRIVATE ${DOLFIN_INCLUDE_DIRS})
    target_link_libraries(dolfincoefficients_pybind PUBLIC dolfincoefficients)
endif ()
