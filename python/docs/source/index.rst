.. dolfincoefficients documentation master file, created by
   sphinx-quickstart on Fri Oct  2 11:03:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dolfincoefficients's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
