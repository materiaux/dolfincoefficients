// Copyright (C) 2019-2020 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#include <functional>
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <dolfincoefficients.h>
#include <dolfin/mesh/Cell.h>

namespace py = pybind11;

namespace dolfincoefficients {

    using array_t = Eigen::ArrayXd;

    PYBIND11_MODULE(dolfincoefficients_pybind, m) {
        m.doc() = "Support for local functions in form for FEniCS/dolfin";

        py::class_<LocalFunction, std::shared_ptr<LocalFunction>>(m, "LocalFunction")
            .def(py::init<const std::function<void(double *values, const double *coefficients,
                                                   const double *constants)> &,
                          std::size_t, std::size_t, std::size_t>())
            .def(py::init([](std::uintptr_t addr, std::size_t result_size,
                             std::size_t coefficients_size, std::size_t constants_size) {
                return LocalFunction{
                    reinterpret_cast<void (*)(double *values, const double *coefficients,
                                              const double *constants)>(addr),
                    result_size, coefficients_size, constants_size};
            }))
            .def_readonly("constants_size", &LocalFunction::constants_size)
            .def_readonly("coefficients_size", &LocalFunction::coefficients_size)
            .def_readonly("result_size", &LocalFunction::result_size)
            .def("__call__", [](const LocalFunction &func, Eigen::Ref<array_t> res,
                                const Eigen::Ref<const array_t> &coefficients,
                                const Eigen::Ref<const array_t> &constants) {
                func(res.data(), coefficients.data(), constants.data());
            });

        py::class_<CoefficientPreprocessor, std::shared_ptr<CoefficientPreprocessor>>(
            m, "CoefficientPreprocessor")
            .def("target_element", &CoefficientPreprocessor::target_element)
            .def("source_element", &CoefficientPreprocessor::source_element)
            .def("__call__", [](const CoefficientPreprocessor &preprocessor,
                                Eigen::Ref<array_t> target, const Eigen::Ref<const array_t> &source,
                                const Eigen::Ref<const array_t> &coordinate_dofs) {
                preprocessor(target.data(), source.data(), coordinate_dofs.data());
            });

        py::class_<Coefficient, std::shared_ptr<Coefficient>>(m, "Coefficient")
            .def(py::init<std::shared_ptr<const dolfin::GenericFunction>>())
            .def(py::init<std::shared_ptr<const dolfin::GenericFunction>,
                          std::shared_ptr<const CoefficientPreprocessor>>())
            .def(py::init<std::shared_ptr<const dolfin::Expression>,
                          std::shared_ptr<const dolfin::FiniteElement>>())
            .def("restrict", // as the fenics/dolfin bindings
                 [](const Coefficient &self, const dolfin::FiniteElement &element,
                    const dolfin::Cell &cell) {
                     ufc::cell ufc_cell;
                     cell.get_cell_data(ufc_cell);

                     std::vector<double> coordinate_dofs;
                     cell.get_coordinate_dofs(coordinate_dofs);

                     std::size_t s_dim = element.space_dimension();
                     Eigen::VectorXd w(s_dim);
                     self.restrict(w.data(), element, cell, coordinate_dofs.data(), ufc_cell);

                     return w; // no copy
                 })
            .def("element", &Coefficient::element)
            .def("dolfin_coefficient", &Coefficient::dolfin_coefficient)
            .def("preprocessor", &Coefficient::preprocessor);

        py::class_<QuadratureExpression, std::shared_ptr<QuadratureExpression>, dolfin::Expression>(
            m, "QuadratureExpression")
            .def(py::init<std::shared_ptr<const LocalFunction>,
                          std::shared_ptr<const dolfin::FiniteElement>,
                          std::vector<std::shared_ptr<const dolfin::GenericFunction>>,
                          std::vector<std::shared_ptr<const dolfin::Constant>>,
                          std::map<std::size_t, std::shared_ptr<const CoefficientPreprocessor>>>())
            .def(py::init<std::shared_ptr<const LocalFunction>,
                          std::shared_ptr<const dolfin::FiniteElement>,
                          std::vector<std::shared_ptr<const Coefficient>>,
                          std::vector<std::shared_ptr<const dolfin::Constant>>>());

        m.def("create_coefficient_preprocessor", [](const std::shared_ptr<const ufc::form> &form) {
            return create_coefficient_preprocessor(form);
        });

        m.def("create_coefficient_preprocessor",
              [](const std::shared_ptr<const ufc::form> &form_a,
                 const std::shared_ptr<const ufc::form> &form_L) {
                  return create_coefficient_preprocessor(form_a, form_L);
              });

        m.def("create_coefficient_preprocessor",
              [](const std::shared_ptr<const dolfin::Form> &form) {
                  return create_coefficient_preprocessor(form);
              });

        m.def("create_coefficient_preprocessor",
              [](const std::shared_ptr<const dolfin::Form> &form_a,
                 const std::shared_ptr<const dolfin::Form> &form_L) {
                  return create_coefficient_preprocessor(form_a, form_L);
              });
    }
} // namespace dolfincoefficients
