# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

import typing
import dolfincoefficients.cpp
import ufl.core.expr
from ufl.algorithms.analysis import extract_coefficients
import dolfin


def mk_cpp_element(element: ufl.FiniteElementBase) -> dolfin.cpp.fem.FiniteElement:
    """Create a dolfin C++ FiniteElement from a UFL FiniteElement"""
    ufc_element, ufc_dofmap = dolfin.jit.jit.ffc_jit(element)
    ufc_element = dolfin.cpp.fem.make_ufc_finite_element(ufc_element)
    return dolfin.cpp.fem.FiniteElement(ufc_element)


def mk_ufl_from_dolfin_cpp_element(element: dolfin.cpp.fem.FiniteElement) -> ufl.FiniteElementBase:
    """Create a UFL FiniteElement from a dolfin C++ FiniteElement"""
    namespace = {}
    exec("from ufl import *", namespace)
    return eval(element.signature(), namespace)


def mk_cpp_element_for_expression(expr: dolfin.Expression, cell_name: str = None) -> dolfin.FiniteElement:
    """Create a dolfin C++ FiniteElement for a dolfin Expression"""
    if expr.cell() is None and cell_name is None:
        raise ValueError("Not all of expr.cell() or given cell_type can be None.")
    element = expr.ufl_element()
    if expr.cell() is None:
        namespace = {}
        exec("from ufl import *", namespace)
        data = repr(element).split(",")
        data[1] = " " + str(cell_name)
        code = ",".join(data)
        element = eval(code, namespace)
    return mk_cpp_element(element)


def mk_cpp_element_for_constant(expr: dolfin.Constant, quadrature_degree: int,
                                quad_scheme: str = "default",
                                cell_name: str = None) -> dolfin.FiniteElement:
    """
    Create a dolfin C++ FiniteElement for a dolfin Constant. This is required because of internals of legacy dolfin.
    """
    if expr.cell() is None and cell_name is None:
        raise ValueError("Not all of expr.cell() or given cell_type can be None.")
    cell_name = cell_name if cell_name is not None else expr.cell()
    return mk_cpp_element(ufl.VectorElement("Quadrature", cell_name, quadrature_degree, quad_scheme=quad_scheme,
                                            dim=expr.ufl_element().reference_value_size()))


def create_coefficient_preprocessor(expr: ufl.core.expr.Expr, target_element: ufl.FiniteElementBase, mesh: dolfin.Mesh,
                                    form_compiler_parameters: dict = None) -> dolfincoefficients.cpp.CoefficientPreprocessor:
    """Create a 'coefficient preprocessor' that locally projects the given UFL expression onto the target_element."""
    assert target_element.family() == "Quadrature"
    form_compiler_parameters = {} if form_compiler_parameters is None else form_compiler_parameters
    dx = ufl.dx(domain=mesh,
                metadata={"quadrature_degree": target_element.degree(),
                          "quadrature_rule": target_element.quadrature_scheme()})
    form_L = ufl.inner(expr, ufl.TestFunction(target_element)) * dx
    ufc_form_L = dolfin.cpp.fem.make_ufc_form(
        dolfin.jit.jit.ffc_jit(form_L, form_compiler_parameters=form_compiler_parameters)[0])
    form_a = ufl.inner(ufl.TrialFunction(target_element), ufl.TestFunction(target_element)) * dx
    ufc_form_a = dolfin.cpp.fem.make_ufc_form(
        dolfin.jit.jit.ffc_jit(form_a, form_compiler_parameters=form_compiler_parameters)[0])
    return dolfincoefficients.cpp.create_coefficient_preprocessor(ufc_form_a, ufc_form_L)


def create_preprocessed_coefficient(expr: ufl.core.expr.Expr, target_element: ufl.FiniteElement,
                                    form_compiler_parameters: dict = None) -> dolfincoefficients.cpp.CoefficientPreprocessor:
    """
    Create a 'coefficient preprocessor' that locally projects the given UFL expression onto the target_element. Thereby,
    the UFL expression must feature exactly one Coefficient which must be a dolfin Function, Constant etc, i.e. provide a
    corresponding 'cpp_object()' member that returns a dolfin::GenericFunction.
    """
    coeffs = extract_coefficients(expr)
    if len(coeffs) != 1:
        raise ValueError("Preprocessed coefficient requires an expression with exactly one coefficient.")
    coeff = extract_coefficients(expr)[0]
    return dolfincoefficients.cpp.Coefficient(
        coeff.cpp_object(),
        create_coefficient_preprocessor(expr, target_element, coeff.function_space().mesh(),
                                        form_compiler_parameters=form_compiler_parameters)
    )


def create_coefficient(V: dolfin.FunctionSpace, name: str = None) -> dolfincoefficients.cpp.Coefficient:
    """
    Create a dolfincoefficients Coefficient on FunctionSpace V, that wraps a newly created dolfin Function over that
    space.
    """
    kwargs = {}
    if name is not None:
        kwargs["name"] = name
    u = dolfin.Function(V, **kwargs)
    return dolfincoefficients.cpp.Coefficient(u._cpp_object)


def create_compiled_quadrature_expression(local_function: dolfincoefficients.cpp.LocalFunction,
                                          target_element: ufl.FiniteElementBase,
                                          coefficients: typing.Iterable[
                                              typing.Union[
                                                  dolfincoefficients.cpp.Coefficient, dolfin.Function, ufl.core.expr.Expr
                                              ]] = None,
                                          constants: typing.Iterable[dolfin.Constant] = None,
                                          form_compiler_parameters: dict = None,
                                          **compiled_expression_kwargs) -> dolfin.CompiledExpression:
    """
    Create a compiled dolfin Expression that evaluates the given LocalFunction on the target_element (of quadrature type).
    The given coefficients and constants have to be evaluated at the same quadrature points as local_function. UFL
    expressions may be given as long as they can be processed via 'create_preprocessed_coefficient.
    """
    result_dolfin_element = mk_cpp_element(target_element)

    def _wrap(item: typing.Union[dolfincoefficients.cpp.Coefficient, dolfin.Function, dolfin.CompiledExpression,
                                 ufl.core.expr.Expr]) -> dolfincoefficients.cpp.Coefficient:
        if isinstance(item, dolfincoefficients.cpp.Coefficient):
            return item
        elif isinstance(item, (dolfin.Function, dolfin.CompiledExpression)):
            return dolfincoefficients.cpp.Coefficient(item.cpp_object())
        elif isinstance(item, ufl.core.expr.Expr):
            pp_element = ufl.TensorElement("Quadrature", target_element.cell(), degree=target_element.degree(),
                                           shape=item.ufl_shape, quad_scheme=target_element.quadrature_scheme())
            return create_preprocessed_coefficient(item, pp_element, form_compiler_parameters=form_compiler_parameters)
        else:
            raise ValueError("Unsupported coefficient type " + str(type(item)))

    coeff_list = [_wrap(coeff) for coeff in coefficients]
    constant_list = [constant.cpp_object() for constant in constants]
    
    quadrature_expression = dolfincoefficients.cpp.QuadratureExpression(local_function, result_dolfin_element,
                                                                        coeff_list,
                                                                        constant_list)
    return dolfin.CompiledExpression(cpp_module=quadrature_expression, element=target_element,
                                     **compiled_expression_kwargs)
