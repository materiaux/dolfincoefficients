# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


"""This module only imports the pybind11 extension"""


# this import is required for properly loading the compiled extension
import dolfin
from .dolfincoefficients_pybind import *
