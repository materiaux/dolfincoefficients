# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

from .version import __version__
