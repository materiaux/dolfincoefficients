# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

import typing
import numpy as np
import dolfin
import dolfincoefficients.cpp
import ufl.core.expr

_function_types = (
    dolfincoefficients.cpp.Coefficient,
    dolfin.Function,
    dolfin.Constant,
    dolfin.Expression,
    dolfin.cpp.function.GenericFunction
)

function_types = typing.Union[
    dolfincoefficients.cpp.Coefficient,
    dolfin.Function,
    dolfin.Constant,
    dolfin.Expression,
    dolfin.cpp.function.GenericFunction
]


def to_dolfin(item: function_types) -> typing.Union[dolfin.Function, dolfin.Constant, dolfin.Expression,
                                                    ufl.core.expr.Expr]:
    """
    Converts 'item' to the appropriate dolfin python type
    """
    if isinstance(item, dolfincoefficients.cpp.Coefficient):
        return dolfin.Function(item.dolfin_coefficient())
    elif isinstance(item, dolfin.cpp.function.Function):
        return dolfin.Function(item)
    elif isinstance(item, dolfin.cpp.function.Constant):
        return dolfin.Constant(item.values())
    elif isinstance(item, (dolfin.Function, dolfin.Constant, dolfin.Expression, ufl.core.expr.Expr)):
        return item
    else:
        raise ValueError("Item of unexpected type {:s}".format(str(type(item))))


def assign(targets: typing.Union[function_types, typing.Iterable[function_types]],
           sources: typing.Union[function_types, typing.Iterable[function_types]]):
    """
    Generalization of dolfin's assign to dolfincoefficient types
    """
    if isinstance(targets, _function_types):
        targets = [targets]
    if isinstance(sources, _function_types):
        sources = [sources]

    def _extract_dolfin_coefficients(items):
        return [to_dolfin(item) for item in items]

    for target, source in zip(_extract_dolfin_coefficients(targets), _extract_dolfin_coefficients(sources)):
        if isinstance(target, dolfin.Constant):
            target.assign(source)
        else:
            dolfin.assign(target, source)


def set_constant_value(target: dolfin.Constant, value: typing.Union[float, np.ndarray]):
    """
    A convenience wrapper to set a Constant's value.
    """
    target.assign(dolfin.Constant(value))


def project_local(expr: ufl.core.expr.Expr, target: typing.Union[function_types, dolfin.FunctionSpace],
                  quadrature_degree: int = None, measure: ufl.Measure = None) -> typing.Union[function_types]:
    """
    An efficient version of element-wise projection
    
    :param expr: the expression to be project
    :param target: the target Function or FunctionSpace
    :param quadrature_degree: the desired quadrature degree
    :param measure: the dolfin/ufl measure providing quadrature and mesh information
    :return: the given target if target is not a function space, otherwise a newly created element of the target space
    """
    if isinstance(target, dolfin.FunctionSpace):
        V = target
        target = dolfin.Function(V)
    else:
        target = to_dolfin(target)
        V = target.function_space()

    if measure is not None and quadrature_degree is not None:
        raise ValueError("Either measure or quadrature_degree can be given, but not both.")
    elif measure is not None:
        dx = measure
    else:
        quadrature_degree = V.ufl_element().degree() if quadrature_degree is None else quadrature_degree
        if "Quadrature" not in V.element().signature():
            quadrature_degree *= 2
        dx = dolfin.Measure("dx", V.mesh(), metadata={"quadrature_degree": quadrature_degree})
    dx_all = dolfin.Measure("dx", V.mesh(), metadata=dx.metadata())
    local_projection = dolfin.LocalSolver(
        ufl.inner(dolfin.TrialFunction(V), dolfin.TestFunction(V)) * dx_all,
        ufl.inner(expr, dolfin.TestFunction(V)) * dx)
    local_projection.solve_local_rhs(target)
    return target


def Evaluator(mesh: dolfin.Mesh, quadrature_degree: int = 0) -> typing.Callable[
    [ufl.core.expr.Expr, typing.Optional[int]], np.ndarray]:
    """
    Create a function object that performs evaluations of expressions over the given mesh via project_local.
    """

    def _func(expr: ufl.core.expr.Expr, quadrature_degree: int = quadrature_degree) -> np.ndarray:
        """
        Evaluate the given expression over the mesh via project_local. Returns the process-local dofs of the projection
        target.
        """
        if len(expr.ufl_shape) == 0:
            V = dolfin.FunctionSpace(mesh, dolfin.FiniteElement("R", mesh.cell_name(), 0))
        else:
            V = dolfin.FunctionSpace(mesh, dolfin.TensorElement("R", mesh.cell_name(), 0, shape=expr.ufl_shape))
        res = project_local(expr, V, quadrature_degree=quadrature_degree)
        return res.vector().get_local()

    return _func
