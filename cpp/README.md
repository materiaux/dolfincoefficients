# C++ library part of dolfincoefficients, a framework agnostic dolfin extension for externally evaluated form coefficients 

This package provides an extension of `dolfin`'s `Expression` class towards support of complex externally provided expressions. 
Python bindings are provided by the Python package `dolfincoefficients`.


## Installation

Standard CMake installation.
Requires `dolfin` version >= 2019.1.0 and `Eigen3` version >= 3.3.7.


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

