#! /usr/bin/env python3

import os
import pathlib

pwd = pathlib.Path.cwd()

print("\n")
print("Creating C++ documentation in {!s}".format(pwd))
print("*"*80 + "\n")

ret = os.system("doxygen")
if ret != 0:
    raise Exception("Some error occurred during doc generation")
