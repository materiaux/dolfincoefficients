// Copyright (C) 2019-2021 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later


#include "dolfincoefficients.h"
#include <dolfin/function/FunctionSpace.h>

#if ((DOLFIN_VERSION_MAJOR == 2019) && (DOLFIN_VERSION_MINOR >= 1))

namespace dolfincoefficients {

    CoefficientPreprocessor
    create_coefficient_preprocessor(const std::shared_ptr<const ufc::form> &form) {
        if (form->rank() != 1 || form->has_cell_integrals() != 1 || form->num_coefficients() != 1)
            throw std::invalid_argument(
                "The given ufc::form is not suitable to create a coefficient preprocessor.");
        std::shared_ptr<const ufc::finite_element> target_element{form->create_finite_element(0)};

        std::size_t target_size = target_element->space_dimension();
        if (target_size != target_element->reference_value_size())
            throw std::invalid_argument("The given ufc::form is not sufficient for the creation of "
                                        "a coefficient preprocessor. "
                                        "In case of more than one quadrature point a bilinear form "
                                        "has to be provided in addition.");

        std::shared_ptr<const ufc::finite_element> source_element{form->create_finite_element(1)};
        std::shared_ptr<const ufc::cell_integral> integral{form->create_default_cell_integral()};
        std::function<void(double *target, const double *source, const double *coordinate_dofs)>
            func = [=](double *target, const double *source, const double *coordinate_dofs) {
                integral->tabulate_tensor(target, &source, coordinate_dofs, 0 /* unused */);
            };
        return CoefficientPreprocessor{
            func, std::make_shared<const dolfin::FiniteElement>(target_element),
            std::make_shared<const dolfin::FiniteElement>(source_element)};
    }

    CoefficientPreprocessor
    create_coefficient_preprocessor(const std::shared_ptr<const ufc::form> &form_a,
                                    const std::shared_ptr<const ufc::form> &form_L) {
        if (form_L->rank() != 1 || form_L->has_cell_integrals() != 1 ||
            form_L->num_coefficients() != 1)
            throw std::invalid_argument(
                "The given RHS ufc::form is not suitable to create a coefficient preprocessor.");
        if (form_a->rank() != 2 || form_a->has_cell_integrals() != 1 ||
            form_a->num_coefficients() != 0)
            throw std::invalid_argument(
                "The given LHS ufc::form is not suitable to create a coefficient preprocessor.");

        std::shared_ptr<const ufc::finite_element> target_element{form_L->create_finite_element(0)};

        std::unique_ptr<const ufc::finite_element> elmt_0_a{form_a->create_finite_element(0)};
        std::unique_ptr<const ufc::finite_element> elmt_1_a{form_a->create_finite_element(1)};
        if (std::string{elmt_0_a->signature()} != std::string{elmt_1_a->signature()})
            throw std::invalid_argument("The given LHS ufc::form is not symmetric.");
        if (std::string{elmt_0_a->signature()} != std::string{target_element->signature()})
            throw std::invalid_argument("The given LHS ufc::form is not compatible with the "
                                        "provided RHS form (different test function spaces).");

        const std::size_t target_size = target_element->space_dimension();
        std::shared_ptr<const ufc::finite_element> source_element{form_L->create_finite_element(1)};
        std::shared_ptr<const ufc::cell_integral> integral_L{
            form_L->create_default_cell_integral()};
        std::shared_ptr<const ufc::cell_integral> integral_a{
            form_a->create_default_cell_integral()};

        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> lhs(target_size,
                                                                                   target_size);

        std::function<void(double *target, const double *source, const double *coordinate_dofs)>
            func = [=](double *target, const double *source,
                       const double *coordinate_dofs) mutable -> void {
            Eigen::Map<Eigen::ArrayXd> rhs(target, target_size);
            lhs.setZero();
            integral_L->tabulate_tensor(target, &source, coordinate_dofs, 0 /* unused */);
            integral_a->tabulate_tensor(lhs.data(), nullptr, coordinate_dofs, 0 /* unused */);
            rhs /= lhs.diagonal().array();
        };
        return CoefficientPreprocessor{
            func, std::make_shared<const dolfin::FiniteElement>(target_element),
            std::make_shared<const dolfin::FiniteElement>(source_element)};
    }

    Coefficient::Coefficient(std::shared_ptr<const dolfin::GenericFunction> dolfin_coefficient,
                             std::shared_ptr<const CoefficientPreprocessor> preprocessor)
        : _dolfin_coefficient{std::move(dolfin_coefficient)},
          _preprocessor{std::move(preprocessor)}, _element{_preprocessor->target_element()},
          expansion_coefficients{std::vector<double>(_preprocessor->source_element()->space_dimension())} {}

    Coefficient::Coefficient(std::shared_ptr<const dolfin::GenericFunction> dolfin_coefficient)
        : _dolfin_coefficient{std::move(dolfin_coefficient)} {
        if (!_dolfin_coefficient->function_space())
            throw std::invalid_argument("If no element or preprocessor is given, the dolfin "
                                        "coefficient must provide a function space");
        _element = _dolfin_coefficient->function_space()->element();
    }

    Coefficient::Coefficient(std::shared_ptr<const dolfin::Expression> dolfin_expression,
                             std::shared_ptr<const dolfin::FiniteElement> element)
        : _dolfin_coefficient{std::move(dolfin_expression)}, _element{std::move(element)} {
        if (!_element)
            throw std::invalid_argument("Nullpointer not allowed for element");
    }

    void Coefficient::restrict(double *w, const dolfin::FiniteElement &element,
                               const dolfin::Cell &dolfin_cell, const double *coordinate_dofs,
                               const ufc::cell &ufc_cell) const {
        if (element.ufc_element()->reference_value_size() !=
            this->element()->ufc_element()->reference_value_size())
            throw std::invalid_argument(
                "The given element must at least have the same reference value dimension.");
        if (_preprocessor) {
            // restrict the coefficient to the current cell (into scratch array)
            _dolfin_coefficient->restrict(expansion_coefficients.data(),
                                          *_preprocessor->source_element(), dolfin_cell,
                                          coordinate_dofs, ufc_cell);
            // Call a preprocessor which represents some projection onto the "quadrature
            // element" and write the result in to the actual coefficient array
            _preprocessor->integral(w, expansion_coefficients.data(), coordinate_dofs);
        } else {
            // restrict the coefficient to the current cell
            _dolfin_coefficient->restrict(w, element, dolfin_cell, coordinate_dofs, ufc_cell);
        }
    }

    std::shared_ptr<const dolfin::FiniteElement> Coefficient::element() const { return _element; }

    std::shared_ptr<const dolfin::GenericFunction> Coefficient::dolfin_coefficient() const {
        return _dolfin_coefficient;
    }
    std::shared_ptr<const CoefficientPreprocessor> Coefficient::preprocessor() const {
        return _preprocessor;
    }

    /// A helper to check whether the given element is of appropriate 'element class'
    bool check_family(const dolfin::FiniteElement &element,
                      const std::vector<std::string> &valid_families) {
        static const std::string mixed{"Mixed"};
        const std::string family{element.ufc_element()->family()};
        if (family == mixed) {
            for (std::size_t ii = 0; ii < element.num_sub_elements(); ++ii) {
                const auto sub = element.create_sub_element(ii);
                if (!check_family(*sub, valid_families))
                    return false;
            }
            return true;
        } else
            return (std::find(begin(valid_families), end(valid_families), family) !=
                    end(valid_families));
    }

    QuadratureExpression::QuadratureExpression(
        std::shared_ptr<const LocalFunction> local_function,
        std::shared_ptr<const dolfin::FiniteElement> element,
        std::vector<std::shared_ptr<const Coefficient>> coefficients,
        std::vector<std::shared_ptr<const dolfin::Constant>> constants)
        : _element{std::move(element)}, _integrand{std::move(local_function)},
          _coefficients{std::move(coefficients)}, _constants{std::move(constants)} {
        using namespace std::string_literals;
        const std::vector<std::string> valid_families{"Quadrature"s, "Real"s};
        if (_element->ufc_element()->reference_value_size() != _integrand->result_size ||
            !check_family(*_element, valid_families)) {
            throw std::invalid_argument("The given element is incompatible with the local_function.");
        }

        assert(_element->ufc_element()->reference_value_size() * num_quadrature_points ==
               _element->ufc_element()->space_dimension());

        std::size_t total_coeff_size = 0;
        for (std::size_t ii = 0; ii < _coefficients.size(); ++ii) {
            total_coeff_size += _coefficients[ii]->element()->ufc_element()->reference_value_size();
            if (_coefficients[ii]->element()->ufc_element()->reference_value_size() *
                    num_quadrature_points !=
                _coefficients[ii]->element()->space_dimension()) {
                throw std::invalid_argument(
                    "Coefficient " + std::to_string(ii) +
                    " is incompatible with the coefficient "
                    "expected from the local_function (differing number of quadrature points).");
            }
        }
        if (_integrand->coefficients_size != total_coeff_size)
            throw std::invalid_argument(
                std::string("Total size of coefficients (") + std::to_string(total_coeff_size) + ") does not match the size expected "
                "by the local function (" + std::to_string(_integrand->coefficients_size) + ").");

        std::size_t total_constants_size = std::accumulate(
            begin(_constants), end(_constants), std::size_t{0},
            [](std::size_t sum, const auto &item) { return sum + item->value_size(); });

        if (_integrand->constants_size != total_constants_size)
            throw std::invalid_argument(
                std::string("Total size of constants (") + std::to_string(total_constants_size) + ") does not match the size expected "
                "by the local function (" + std::to_string(_integrand->constants_size) + ").");

        coeff_array.resize(_integrand->coefficients_size * num_quadrature_points);
        qp_coeff_array.resize(_integrand->coefficients_size);
        qp_constants_array.resize(_integrand->constants_size);
        qp_A.resize(_integrand->result_size);
    }

    /// Create a vector of dolfincoefficients::Coefficient from 
    /// dolfin::GenericFunction objects and dolfincoefficients::CoefficientPreprocessors.
    ///
    /// @param[in] coefficients Vector of coefficients, which might need preprocessing
    /// @param[in] coefficient_preprocessors Map (coefficient index -> preprocessor) that 
    /// provides preprocessors for the indicated elements of \p coefficients
    /// return The created dolfincoefficients::Coefficient vector
    std::vector<std::shared_ptr<const Coefficient>>
    make_coefficients(std::vector<std::shared_ptr<const dolfin::GenericFunction>> coefficients,
                      std::map<std::size_t, std::shared_ptr<const CoefficientPreprocessor>>
                          coefficient_preprocessors) {
        std::vector<std::shared_ptr<const Coefficient>> _coefficients;
        for (std::size_t ii = 0; ii < coefficients.size(); ++ii) {
            auto kv = coefficient_preprocessors.find(ii);
            if (kv != coefficient_preprocessors.end()) {
                bool compatible = true;
                if (coefficients[ii]->function_space()) {
                    if (coefficients[ii]->function_space()->element()->hash() !=
                        kv->second->source_element()->hash())
                        compatible = false;
                } else {
                    if (coefficients[ii]->value_size() !=
                        kv->second->source_element()->ufc_element()->space_dimension())
                        compatible = false;
                }
                if (!compatible)
                    throw std::invalid_argument("coefficient " + std::to_string(ii) +
                                                " is incompatible with its preprocessor");
                _coefficients.emplace_back(
                    std::make_shared<const Coefficient>(coefficients[ii], kv->second));
            } else {
                _coefficients.emplace_back(std::make_shared<const Coefficient>(coefficients[ii]));
            }
        }
        return _coefficients;
    }

    QuadratureExpression::QuadratureExpression(
        std::shared_ptr<const LocalFunction> local_function,
        std::shared_ptr<const dolfin::FiniteElement> element,
        std::vector<std::shared_ptr<const dolfin::GenericFunction>> coefficients,
        std::vector<std::shared_ptr<const dolfin::Constant>> constants,
        std::map<std::size_t, std::shared_ptr<const CoefficientPreprocessor>>
            coefficient_preprocessors)
        : QuadratureExpression{
              std::move(local_function), std::move(element),
              make_coefficients(std::move(coefficients), std::move(coefficient_preprocessors)),
              std::move(constants)} {}

    // Restrict function to a cell
    void QuadratureExpression::restrict(double *w, const dolfin::FiniteElement &element,
                                        const dolfin::Cell &dolfin_cell,
                                        const double *coordinate_dofs,
                                        const ufc::cell &ufc_cell) const {
        using vector_t = Eigen::Array<double, Eigen::Dynamic, 1>;
        using matrix_t = Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
        Eigen::Map<matrix_t> Eigen_coeff_array{coeff_array.data(),
                                               static_cast<Eigen::Index>(_integrand->coefficients_size),
                                               static_cast<Eigen::Index>(num_quadrature_points)};

        Eigen::Map<vector_t> Eigen_qp_coeff_array{qp_coeff_array.data(),
                                                  static_cast<Eigen::Index>(qp_coeff_array.size())};

        Eigen::Map<vector_t> Eigen_qp_A{qp_A.data(), static_cast<Eigen::Index>(qp_A.size())};

        if (element.hash() != _element->hash())
            throw std::invalid_argument(
                "QuadratureExpression can only be restricted to its own element " +
                _element->signature());

        // prepare coefficients for whole cell
        Eigen_coeff_array.setZero();
        std::size_t offset = 0;
        // loop over all coefficients
        for (const auto &_coefficient : _coefficients) {
            _coefficient->restrict(coeff_array.data() + offset, *_coefficient->element(),
                                   dolfin_cell, coordinate_dofs, ufc_cell);
            offset += _coefficient->element()->space_dimension();
        }

        // loop over all constants
        offset = 0;
        for (const auto &_constant : _constants) {
            const auto &values = _constant->values();
            std::copy(begin(values), end(values), qp_constants_array.data() + offset);
            offset += _constant->value_size();
        }

        // the actual restriction using the integrand
        // qp-local arrays
        Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> w_map(
            w, _integrand->result_size, num_quadrature_points);

        // do the restriction by point-wise evaluations
        for (int qi = 0; qi < num_quadrature_points; ++qi) {
            Eigen_qp_A.setZero();
            // qp-local coefficients
            Eigen_qp_coeff_array = Eigen_coeff_array.col(qi);
            // call the integrand
            _integrand->func(qp_A.data(), qp_coeff_array.data(), qp_constants_array.data());
            // write point data into quadrature coefficient
            w_map.col(qi) = Eigen_qp_A;
        }
    }

} // namespace dolfincoefficients

#endif
