// Copyright (C) 2019-2021 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/dolfincoefficients)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later


/// \mainpage dolfincoefficients C++ interface documentation
/// Welcome to the C++ internals of dolfincoefficients. 
/// This project provides extensions around <a href="https://fenicsproject.org">FEniCS</a> (dolfin's) Expression class
/// that enable the use of 'external functions', i.e. a specific material 
/// response, such as those provided by the <a href="https://gitlab.com/materiaux/materiaux">materiaux</a> package.


#pragma once

#include <dolfin/common/version.h>
#if ((DOLFIN_VERSION_MAJOR == 2019) && (DOLFIN_VERSION_MINOR >= 1))

#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/Form.h>
#include <dolfin/function/Constant.h>
#include <dolfin/function/Expression.h>
#include <dolfin/function/Function.h>
#include <functional>


/// Toplevel namespace
namespace dolfincoefficients {

    /// \addtogroup dolfincoefficients
    /// @{  

    
    /// Represents local functions, e.g. material responses or PDE coefficients

    /// Essential information about the local function, that is the function itself and the length
    /// of its results, all coefficients and constants. Of the latter two, only the sum is provided.
    /// This information can be used for crosschecks with in the construction of a
    /// QuadratureExpression.
    struct LocalFunction {

        /// Constructor
        ///
        /// @param func The underlying function
        /// @param result_size required size of an array/vector holding the result
        /// @param coefficients_size required size of an array/vector holding the coefficients
        /// @param constants_size required size of an array/vector holding the constants
        LocalFunction(const std::function<void(double *values, const double *coefficients,
                                           const double *constants)> &func,
                  std::size_t result_size, std::size_t coefficients_size,
                  std::size_t constants_size)
            : func{func}, result_size{result_size}, coefficients_size{coefficients_size},
              constants_size{constants_size} {}

        /// Stores the local function(callable)
        const std::function<void(double *values, const double *coefficients,
                                 const double *constants)>
            func;
        
        /// Size (length of an array/vector) to store the results
        const std::size_t result_size;
        
        /// Size (length of an array/vector) to store the all numerical coefficient data
        const std::size_t coefficients_size;
        
        /// Size (length of an array/vector) to store the all numerical constant data
        const std::size_t constants_size;

        /// Call operator
        ///
        /// @param[out] values array to which the results is written
        /// @param[in] coefficients array of coefficients of the 'function'
        /// @param[in] constants array of constants of the 'function'
        void operator()(double *values, const double *coefficients, const double *constants) const {
            func(values, coefficients, constants);
        }
    };


    /// This class can be used to lazily evaluate an expression (given, e.g., in terms of a projection) that is intended as an argument for a LocalFunction.
    struct CoefficientPreprocessor {

        /// Constructor; rather low-level. Consider using the factories ::create_coefficient_preprocessor.
        ///
        /// @param[in] integral The function performing the "projection". The signature corresponds to a reduced interface
        ///     version of FEniCS/dolfin tabulate_tensor functions.
        /// @param[in] target_element FiniteElement representing the target space
        /// @param[in] source_element FiniteElement representing the source space
        CoefficientPreprocessor(const std::function<void(double *target, const double *source,
                                                         const double *coordinate_dofs)> &integral,
                                std::shared_ptr<const dolfin::FiniteElement> target_element,
                                std::shared_ptr<const dolfin::FiniteElement> source_element)
            : integral{integral}, _target_element{std::move(target_element)},
              _source_element{std::move(source_element)} {}

        /// Stores the callable that does the projection (preprocessor operation)
        const std::function<void(double *target, const double *source,
                                 const double *coordinate_dofs)>
            integral;

        /// Call operator
        ///
        /// @param[out] target Array to which the projection result is written
        /// @param[in] source Input (coefficient values) to the projection
        /// @param[in] coordinate_dofs Coordinate of the cell on which the projection is to be perfomed
        void operator()(double *target, const double *source, const double *coordinate_dofs) const {
            integral(target, source, coordinate_dofs);
        }

        /// Get the target element
        [[nodiscard]] std::shared_ptr<const dolfin::FiniteElement> target_element() const {
            return _target_element;
        }

        /// Get the source element
        [[nodiscard]] std::shared_ptr<const dolfin::FiniteElement> source_element() const {
            return _source_element;
        }

      private:
        std::shared_ptr<const dolfin::FiniteElement> _target_element;
        std::shared_ptr<const dolfin::FiniteElement> _source_element;
    };

    /// Factory for a preprocessors taking a single ufc form corresponding to the linear form of a
    /// projection. This only works for integrals with a single quadrature point where the division through the
    /// cell volume is part of the linear form.
    ///
    /// @param[in] form The linear form part of the projection already weighted by cell volume
    /// @return The CoefficientPreprocessor instance that represents a Functor performing the projection
    CoefficientPreprocessor
    create_coefficient_preprocessor(const std::shared_ptr<const ufc::form> &form);

    /// Factory for a preprocessors taking a the two forms building
    /// a projection.
    ///
    /// @param[in] form_a The bilinear form
    /// @param[in] form_L The linear form
    /// @return The CoefficientPreprocessor instance that represents a Functor performing the projection
    CoefficientPreprocessor
    create_coefficient_preprocessor(const std::shared_ptr<const ufc::form> &form_a,
                                    const std::shared_ptr<const ufc::form> &form_L);

    /// Create Preprocessor. Version taking a dolfin Form
    inline CoefficientPreprocessor
    create_coefficient_preprocessor(const std::shared_ptr<const dolfin::Form> &form) {
        return create_coefficient_preprocessor(form->ufc_form());
    }

    /// Create Preprocessor. Version taking two dolfin Forms
    CoefficientPreprocessor
    create_coefficient_preprocessor(const std::shared_ptr<const dolfin::Form> &form_a,
                                    const std::shared_ptr<const dolfin::Form> &form_L) {
        return create_coefficient_preprocessor(form_a->ufc_form(), form_L->ufc_form());
    }

    /// Class representing a coefficient (input) of a local function.
    class Coefficient {
      public:

        /// Constructor for a coefficient based on preprocessor and its input coefficient
        Coefficient(std::shared_ptr<const dolfin::GenericFunction> dolfin_coefficient,
                    std::shared_ptr<const CoefficientPreprocessor> preprocessor);

        /// Constructor simply holding an appropriate dolfin GenericFunction
        explicit Coefficient(std::shared_ptr<const dolfin::GenericFunction> dolfin_coefficient);

        /// Constructor simply holding a dolfin Expression and the element to which it should be
        /// restricted.
        Coefficient(std::shared_ptr<const dolfin::Expression> dolfin_expression,
                    std::shared_ptr<const dolfin::FiniteElement> element);

        // Restriction to a cell. This interface mirrors dolfin::GenericFunction::restrict.
        ///
        /// @param[out] w The computed 'expansion coefficients'
        /// @param[in] element The element (space) to which the coefficient is to be restricted
        /// @param[in] dolfin_cell The dolfin cell (geometry information interface) where the restriction happens
        /// @param[in] coordinate_dofs The cell coordinates
        /// @param[in] ufc_cell The ufc cell (geometry information interface) where the restriction happens
        void restrict(double *w, const dolfin::FiniteElement &element,
                      const dolfin::Cell &dolfin_cell, const double *coordinate_dofs,
                      const ufc::cell &ufc_cell) const;

        /// Get the coefficient element
        std::shared_ptr<const dolfin::FiniteElement> element() const;

        /// Get the stored dolfin coefficient
        std::shared_ptr<const dolfin::GenericFunction> dolfin_coefficient() const;

        /// Get the stored dolfin coefficient (null if none is there)
        std::shared_ptr<const CoefficientPreprocessor> preprocessor() const;

      private:
        std::shared_ptr<const dolfin::GenericFunction> _dolfin_coefficient{};

        std::shared_ptr<const CoefficientPreprocessor> _preprocessor{};
        std::shared_ptr<const dolfin::FiniteElement> _element{};
        mutable std::vector<double> expansion_coefficients{};
    };




    /// The main class of this package. It collects the local function, coefficients, constants and preprocessors. 
    
    /// From the object given it forms the actual restrictable (to quadrature points) expression
    /// that can be used in a fenics/ufl expression or form.
    class QuadratureExpression final : public dolfin::Expression {
      public:

        /// Constructor taking the local function to be evaluation per quadrature point, the element
        /// of the Expression, the coefficients used as input, constants for the local function
        /// and preprocessors in the form {coeffindex, preprocessor} where coeffindex refers to the
        /// index in the coefficient vector. This coefficient will be used as input for the
        /// preprocessor.
        ///
        /// @param[in] local_function The function to be evaluated locally (per quadrature point)
        /// @param[in] element The element of the expression. Must be a 'quadrature' element.
        /// @param[in] coefficients The coefficients (un-processed) of the expression
        /// @param[in] constants The constants of the local function
        /// @param[in] coefficient_preprocessors Preprocessors for the coefficients provided to obtain the actual
        ///     coefficients as appropriate for the local function. The key of the map refers
        ///     to the position of the coefficient to be pre-processed in the 'coefficients' vector.
        QuadratureExpression(
            std::shared_ptr<const LocalFunction> local_function,
            std::shared_ptr<const dolfin::FiniteElement> element,
            std::vector<std::shared_ptr<const dolfin::GenericFunction>> coefficients,
            std::vector<std::shared_ptr<const dolfin::Constant>> constants,
            std::map<std::size_t, std::shared_ptr<const CoefficientPreprocessor>>
                coefficient_preprocessors);

        /// Constructor. In this version, preprocessors are already contained in coefficients.
        ///
        /// @param[in] local_function The function to be evaluated locally (per quadrature point)
        /// @param[in] element The element of the expression. Must be a 'quadrature' element.
        /// @param[in] coefficients The coefficients of the local function
        /// @param[in] constants The constants of the local function
        QuadratureExpression(std::shared_ptr<const LocalFunction> local_function,
                             std::shared_ptr<const dolfin::FiniteElement> element,
                             std::vector<std::shared_ptr<const Coefficient>> coefficients,
                             std::vector<std::shared_ptr<const dolfin::Constant>> constants);

        /// Throws. This operation is meaningful for quadrature type functions.
        void eval(Eigen::Ref<Eigen::VectorXd> values, Eigen::Ref<const Eigen::VectorXd> x,
                  const ufc::cell &cell) const override {
            throw std::runtime_error(
                "This class is not intended for point evaluations. It only implements 'restrict'.");
        }

        /// Throws. This operation is meaningful for quadrature type functions.
        void compute_vertex_values(std::vector<double> &, const dolfin::Mesh &) const override {
            throw std::runtime_error(
                "This class is not intended for point evaluations. It only implements 'restrict'.");
        }

        /// Returns an empty function space. In general, one does not want to allocate a full
        /// function space for such an expression.
        std::shared_ptr<const dolfin::FunctionSpace> function_space() const override {
            return std::shared_ptr<const dolfin::FunctionSpace>{};
        }

        /// Restrict function to a cell
        void restrict(double *w, const dolfin::FiniteElement &element,
                      const dolfin::Cell &dolfin_cell, const double *coordinate_dofs,
                      const ufc::cell &ufc_cell) const override;

        /// Value rank
        std::size_t value_rank() const override { return _element->value_rank(); }

        /// Value dimension
        std::size_t value_dimension(std::size_t i) const override {
            return _element->value_dimension(i);
        }

        /// Value shape
        std::vector<std::size_t> value_shape() const override {
            std::vector<std::size_t> shape;
            for (std::size_t ii = 0; ii < value_rank(); ++ii)
                shape.push_back(value_dimension(ii));
            return shape;
        }

      private:
        std::shared_ptr<const dolfin::FiniteElement> _element;
        std::shared_ptr<const LocalFunction> _integrand;
        std::vector<std::shared_ptr<const Coefficient>> _coefficients;
        std::vector<std::shared_ptr<const dolfin::Constant>> _constants;
        std::size_t quadrature_dofs{_element->ufc_element()->space_dimension()};
        std::size_t num_quadrature_points{quadrature_dofs /
                                          _element->ufc_element()->reference_value_size()};
        mutable std::vector<double> coeff_array{};
        mutable std::vector<double> qp_coeff_array{};
        mutable std::vector<double> qp_constants_array{};
        mutable std::vector<double> qp_A{};
//        mutable Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> coeff_array{
//            _integrand->coefficients_size, num_quadrature_points};
//        mutable Eigen::Array<double, Eigen::Dynamic, 1> qp_coeff_array{
//            _integrand->coefficients_size};
//        mutable Eigen::Array<double, Eigen::Dynamic, 1> qp_constants_array{
//            _integrand->constants_size};
//        mutable Eigen::Array<double, Eigen::Dynamic, 1> qp_A{_integrand->result_size};
    };

    /// @} End of doxygen group dolfincoefficients
} // namespace dolfincoefficients

#endif
