# v0.3: Update marking compatibility with materiaux v0.3

 * minor fixes in factories
 
 * formatting


# v0.2: Update marking compatibility with materiaux v0.2

 * remove Eigen3 types from public C++ header



# v0.1: initial release

 * functionality is there, actively used for research
