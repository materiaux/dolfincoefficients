# dolfincoefficients, a framework agnostic dolfin extension for externally evaluated form coefficients 

`dolfincoefficients` comes with a C++ and a Python package, whereas the latter depends on the former. 
For details on each package visit the respective directories. This project is part of [`Materiaux`](https://gitlab.com/materiaux).

Ready-to-run containers can be found in the [container registry](https://gitlab.com/materiaux/containers/container_registry/) 
of [`Materiaux Containers`](https://gitlab.com/materiaux/containers).


## License

License information is available in the package directories `cpp` and `python`.
